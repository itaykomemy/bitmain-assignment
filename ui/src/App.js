import React, {Component} from 'react'
import If from './components/If'
import Loader from './components/Loader'
import MatchModalRenderer from './components/MatchModalRenderer'
import Modal from './components/Modal'
import AppContainer from './components/styled/containers/FlexContainer'
import PageHeader from './components/styled/PageHeader'
import Table from './components/Table'
import OrderDatabase from './orders/OrderDatabase'
import OrderPoller from './orders/OrderPoller'
import type {MatchType} from './types'
import {top20} from './utilities/arrayUtils'
import {formatDate} from './utilities/dateUtils'

const MAX_RESPONSE_LENGTH = 100,
    POLLING_INTERVAL = 1000

class App extends Component {

    _poller = new OrderPoller('http://localhost:5001/listOrders', 1, MAX_RESPONSE_LENGTH)
    _db = OrderDatabase

    state = {
        sellOrders: [],
        buyOrders: [],
        matches: [],
        selectedMatch: null,
        uiSynced: false,
        error: null,
    }

    openModal = (match: MatchType) => {
        this.setState({
            selectedMatch: match,
        })
    }

    closeModal = () => {
        this.setState({
            selectedMatch: null,
        })
    }

    componentWillMount() {
        this._poller.startPolling(
            response => {
                this._db.update(response)

                if (!this.state.uiSynced && response.length < MAX_RESPONSE_LENGTH) {
                    // UI is synced with latest server orders
                    this.setState({
                        uiSynced: true,
                    })
                    this._poller.changeInterval(POLLING_INTERVAL)
                }

                if (this.state.uiSynced) {
                    this.setState({
                        sellOrders: this._db.getSellOrders(),
                        buyOrders: this._db.getBuyOrders(),
                        matches: this._db.getLatestMatches(),
                    })
                }
            },
            error => this.setState({error})
        )
    }

    render() {
        const {uiSynced, selectedMatch} = this.state

        return (
            <React.Fragment>
                <Loader show={!uiSynced}/>
                <PageHeader>
                    <h1>Trades and Orders</h1>
                </PageHeader>
                <AppContainer>
                    <Table
                        title="Sell Orders"
                        columnDefinitions={[
                            {title: 'ID', property: 'id'},
                            {title: 'Price', property: 'price'},
                            {title: 'Amount', property: 'quantity'},
                        ]}
                        data={this.state.sellOrders.filter(top20)}
                    />

                    <Table
                        title="Buy Orders"
                        columnDefinitions={[
                            {title: 'ID', property: 'id'},
                            {title: 'Price', property: 'price'},
                            {title: 'Amount', property: 'quantity'},
                        ]}
                        data={this.state.buyOrders.filter(top20)}
                    />

                    <Table
                        title="Trades"
                        columnDefinitions={[
                            {title: 'Created', property: 'created', renderer: formatDate},
                            {title: 'Price', property: 'price'},
                            {title: 'Amount', property: 'quantity'},
                        ]}
                        data={this.state.matches}
                        onRowClickHandler={match => this.openModal(match)}
                    />

                    <If condition={selectedMatch}>
                        <Modal onClose={this.closeModal}>
                            <h1>Match Details</h1>
                            <MatchModalRenderer match={selectedMatch}/>
                        </Modal>
                    </If>
                </AppContainer>
            </React.Fragment>
        )
    }
}

export default App
