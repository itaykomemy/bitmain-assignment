import PropTypes from 'prop-types'

const If = ({children, condition}) => {
    if (condition) {
        return children
    } else {
        return null
    }
}

If.propTypes = {
    condition: PropTypes.bool,
}
If.defaultProps = {
    condition: false
}

export default If
