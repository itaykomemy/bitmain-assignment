import React from 'react'
import LoaderContainer from './styled/loader/LoaderContainer'
import Spinner from './styled/loader/Spinner'


export default ({show}) => {
    if (show) {
        return (
            <LoaderContainer>
                <Spinner />
            </LoaderContainer>
        )
    }
    else {
        return null
    }
}
