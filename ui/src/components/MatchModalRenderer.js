import React from 'react'
import type {MatchType} from '../types'
import {formatDate} from '../utilities/dateUtils'
import FlexContainer from './styled/containers/FlexContainer'
import ValueBox from './ValueBox'


// created: Date,
//     quantity: number,
//     price: number,
//     buyOrder: OrderType,
//     sellOrder: OrderType,

function MatchModalRenderer({match}: { match: MatchType }) {
    if (!match) return null

    return (
        <FlexContainer>
            <ValueBox
                title="Created"
                value={formatDate(match.created)}
            />

            <ValueBox
                title="Amount"
                value={match.quantity
                }
            />

            <ValueBox
                title="Price"
                value={match.price}
            />

            <ValueBox
                title={`Buy Order (${match.buyOrder.id})`}
                value={`${match.buyOrder.quantity} @ ${match.buyOrder.price}`}
            />

            <ValueBox
                title={`Sell Order (${match.sellOrder.id})`}
                value={`${match.sellOrder.quantity} @ ${match.sellOrder.price}`}
            />

        </FlexContainer>
    )
}

export default MatchModalRenderer
