import PropTypes from 'prop-types'
import React, {Component} from 'react'
import ReactDOM from 'react-dom'
import ModalContainer from './styled/modal/ModalContainer'
import ModalContent from './styled/modal/ModalContent'

class Modal extends Component {
    static propTypes = {
        onClose: PropTypes.func.isRequired
    }
    static defaultProps = {}

    constructor(props) {
        super(props)
        this.el = document.createElement('div')
    }

    componentWillMount() {
        document.body.appendChild(this.el)
    }

    componentWillUnmount() {
        document.body.removeChild(this.el)
    }

    render() {
        const {onClose} = this.props
        return ReactDOM.createPortal(
            (
                <ModalContainer onClick={onClose}>
                    <ModalContent onClick={e => e.stopPropagation()}>
                        {this.props.children}
                    </ModalContent>
                </ModalContainer>
            ),
            this.el
        )
    }
}

export default Modal