import PropTypes from 'prop-types'
import React, {Component} from 'react'
import {
    CSSTransition,
    TransitionGroup
} from 'react-transition-group'
import {DefaultTableStyle} from './styled/tables/StyledTable'
import {
    Cell,
    HeaderCell
} from './styled/tables/TableCells'
import {
    HeaderRow,
    Row
} from './styled/tables/TableRows'

const noop = () => {
}

class Table extends Component {
    static propTypes = {
        data: PropTypes.array,
        title: PropTypes.string,
        columnDefinitions: PropTypes.array,
        onRowClickHandler: PropTypes.func
    }

    static defaultProps = {
        data: [],
        columnDefinitons: [],
        onRowClickHandler: noop,
    }

    render() {
        const {columnDefinitions, data, title, onRowClickHandler} = this.props
        return (
            <DefaultTableStyle>
                <thead>
                <HeaderRow>
                    <HeaderCell>{title}</HeaderCell>
                </HeaderRow>
                <HeaderRow>
                    {
                        columnDefinitions.map(
                            (def, index) =>
                                <HeaderCell key={index}>{def.title}</HeaderCell>
                        )
                    }
                </HeaderRow>
                </thead>

                <TransitionGroup
                    timeout={500}
                    component='tbody'
                >
                    {
                        data.map(datum =>
                            <CSSTransition
                                key={datum.id}
                                timeout={500}
                                classNames="fade">
                                <Row
                                    onClick={() => onRowClickHandler(datum)}
                                    clickable={onRowClickHandler !== noop}>
                                    {
                                        columnDefinitions.map((def, index) => {
                                                const {property, renderer} = def
                                                const data = datum[property]
                                                return <Cell key={index}>{renderer ? renderer(data) : data}</Cell>
                                            }
                                        )
                                    }
                                </Row>
                            </CSSTransition>
                        )
                    }
                </TransitionGroup>
            </DefaultTableStyle>
        )
    }
}


export default Table
