import PropTypes from 'prop-types'
import React from 'react'
import {
    ValueBoxContainer,
    ValueBoxTitle,
    ValueBoxValue
} from './styled/value_boxes'

const ValueBox = ({title, value}) => {
    return (
        <ValueBoxContainer>
            <ValueBoxTitle>
                {title}
            </ValueBoxTitle>
            <ValueBoxValue>
                {value}
            </ValueBoxValue>
        </ValueBoxContainer>
    )
}

ValueBox.propTypes = {
    title: PropTypes.string,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number
    ]),
}

ValueBox.defaultProps = {}

export default ValueBox







