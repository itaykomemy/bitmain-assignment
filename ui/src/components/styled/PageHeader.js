import styled from 'styled-components'

export default styled.div`
  background: ${props => props.theme.primary};
  color: ${props => props.theme.secondary};
  height: 70px;
  line-height: 70px;
  padding-left: 100px;
  margin-bottom: 20px;
  h1 {
    margin: 0;
  }
`
