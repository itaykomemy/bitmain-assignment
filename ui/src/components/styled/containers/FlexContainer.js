import styled from 'styled-components'

export default styled.div`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: ${props => props.justify || 'space-around'};
  margin: 0 auto;
  
  @media(min-width: 1024px) {
    max-width: 1024px;
  }
`
