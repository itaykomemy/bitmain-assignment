import styled from 'styled-components'

export default styled.div`
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  bottom: 0;
  
  background-color: rgba(255,255,255, .85);
 
  z-index: 2;
`
