import styled from 'styled-components'

export default styled.div`
    opacity: 1;
    border: 16px solid ${props => props.theme.primary}; /* Light grey */
    border-top: 16px solid ${props => props.theme.secondary}; /* Blue */
    border-radius: 50%;
    width: 120px;
    height: 120px;
    animation: spin 2s linear infinite;
    position: absolute;
    top: calc(50% - 60px);
    left: calc(50% - 60px);

    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
`
