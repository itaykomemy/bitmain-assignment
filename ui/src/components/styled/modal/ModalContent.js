import styled from 'styled-components'

export default styled.div`
  margin: 20vh auto 0 auto;
  min-height: 200px;
  max-width: 768px;
  background-color: white;
  border-radius: 5px;
  padding: 20px;  
  text-align: center;
`
