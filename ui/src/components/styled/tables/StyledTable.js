import styled from 'styled-components'

const DefaultTableStyle = styled.table`
  text-align: left;
  border-collapse: collapse;
  margin-bottom: 20px;
`

export {DefaultTableStyle}