import styled from 'styled-components'

const HeaderCell = styled.th`
  color: ${props => props.theme.secondary};
  padding: 10px 20px;
`

const Cell = styled.td`
  color: ${props => props.theme.primary};
  padding: 10px 20px;
`

export {HeaderCell, Cell}