import styled, {css} from 'styled-components'
import {fadeAnimations} from '../utils/fade-in-out'

const HeaderRow = styled.tr`
  color: gray;
  background: ${props => props.theme.primary};
`

const clickableRow = css`
  :hover {
    background-color: ${props => props.theme.highlight};    
    cursor: pointer;
  }
`




const Row = styled.tr`
  background: ${props => props.theme.secondary};
  border-right: none;
  border-left: none;
  border-top: 1px solid white;
  ${props => props.clickable && clickableRow};
  ${fadeAnimations};
`

export {
    HeaderRow,
    Row
}
