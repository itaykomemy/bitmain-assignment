import styled from 'styled-components'

const ValueBoxContainer = styled.div`
  border: 2px solid ${props => props.theme.primary};
  margin: 20px;
`

const ValueBoxTitle = styled.div`
  padding: 5px;  
  background-color: ${props => props.theme.primary};
  color: ${props => props.theme.secondary};
`

const ValueBoxValue = styled.div`
  padding: 5px;  
`

export {
    ValueBoxContainer,
    ValueBoxTitle,
    ValueBoxValue
}