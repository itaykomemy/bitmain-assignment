import React from 'react'
import ReactDOM from 'react-dom'
import {ThemeProvider} from 'styled-components'
import App from './App'
import './index.css'

const theme = {
    primary: 'rgba(57, 62, 65, 1)',
    secondary: 'rgba(211, 208, 203, 1)',
    highlight: 'rgba(226, 192, 68, 1)',
    $color4: 'rgba(88, 123, 127, 1)',
    $color5: 'rgba(30, 32, 25, 1)',
}

ReactDOM.render(
    <ThemeProvider theme={theme}>
        <App/>
    </ThemeProvider>,
    document.getElementById('root'))
