// @flow

import type {OrderType} from '../types'
import {top30} from '../utilities/arrayUtils'
import * as OrderMatcher from './OrderMatcher'
import OrderSorter from './OrderSorter'

class OrderDatabase {
    sellOrders: Array<OrderType> = []
    buyOrders: Array<OrderType> = []
    matches = []

    matcher = OrderMatcher

    update(orders: Array<OrderType>) {
        const newSellOrders = orders.filter(isSellOrder)
        const newBuyOrders = orders.filter(isBuyOrder)

        this.sellOrders = this.sellOrders.concat(newSellOrders).sort(OrderSorter)
        this.buyOrders = this.buyOrders.concat(newBuyOrders).sort(OrderSorter)

        this.match()
    }

    match() {
        const matches = this.matcher.findMatches(this.sellOrders, this.buyOrders)

        this.sellOrders = this.sellOrders.filter(isPositiveQuantity)
        this.buyOrders = this.buyOrders.filter(isPositiveQuantity)

        this.matches = matches.concat(this.matches).filter(top30)
    }

    getSellOrders() {
        return this.sellOrders
    }

    getBuyOrders() {
        return this.buyOrders
    }

    getLatestMatches() {
        return this.matches
    }
}


/* singleton */
export default new OrderDatabase()


const isSellOrder = (order: OrderType): boolean =>
    order.type === 'sell'

const isBuyOrder = (order: OrderType): boolean =>
    order.type === 'buy'

const isPositiveQuantity = (order: OrderType): boolean =>
    order.quantity > 0