// @flow
import type {
    MatchType,
    OrderType
} from '../types'

/*

When there is a buy order whose price is higher than another sell order's price, these two orders can be matched.

The price is the average between the buy and the sell order.

When a match can be made with multiple orders, the higher price will get first priority

If two orders have same price, the oldest order (with the lowest id) will have higher priority.

* */


const findMatches = (sellOrders: Array<OrderType>, buyOrders: Array<OrderType>): Array<MatchType> => {
    const matches = []
    let buyIndex = 0, sellIndex = 0

    while (buyIndex < buyOrders.length && sellIndex < sellOrders.length) {
        const buyOrder = buyOrders[buyIndex]
        const sellOrder = sellOrders[sellIndex]

        if (buyOrder.price >= sellOrder.price) {
            // match
            const avgPrice = average(buyOrder.price, sellOrder.price)
            const matchQuantity = Math.min(buyOrder.quantity, sellOrder.quantity)

            matches.push({
                id: `${buyOrder.id}${sellOrder.id}`,
                created: new Date(),
                quantity: matchQuantity,
                price: avgPrice,
                buyOrder: copyOrder(buyOrder),
                sellOrder: copyOrder(sellOrder)})

            buyOrder.quantity -= matchQuantity
            sellOrder.quantity -= matchQuantity

            if (buyOrder.quantity === 0) {
                buyIndex += 1
            }

            if (sellOrder.quantity === 0) {
                sellIndex += 1
            }

        } else {
            sellIndex += 1
        }
    }

    return matches
}

const copyOrder = (order: OrderType): OrderType => Object.assign({}, order)

const average = (a: number, b: number) => (a + b) / 2

export {findMatches}
