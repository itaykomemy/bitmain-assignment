// @flow
import axios from 'axios'
import type {OrderType} from '../types'

class OrderPoller {
    _url: string
    _start: number
    _limit: number
    _interval: number
    _timeoutId: TimeoutID

    constructor(url: string, start: number, limit: number) {
        this._url = url
        this._start = start
        this._limit = limit
    }

    startPolling(onSuccess: (Array<OrderType>) => void, onError: (any) => void, interval: number = 0) {
        this._interval = interval
        if (this._timeoutId) throw Error("Already Polling")

       const handler = () => {
           this._timeoutId = setTimeout(
               () => axios.get(this._url + `?start=${this._start}&size=${this._limit}`)
                   .then(response => {
                       this._start += response.data.length
                       handler()
                       return response.data
                   })
                   .then(onSuccess)
                   .catch(error => {
                       handler()
                       onError(error)
                   })
               , this._interval
           )
       }

       handler()
    }

    changeInterval(interval: number) {
        this._interval = interval
    }

    stopPolling() {
        clearTimeout(this._timeoutId)
    }
}


export default OrderPoller
