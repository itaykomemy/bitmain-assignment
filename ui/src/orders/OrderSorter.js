// @flow
import type {OrderType} from '../types'

export default function(orderA: OrderType, orderB: OrderType): number {
    // compare price
    const priceDiff = orderB.price - orderA.price
    if (priceDiff === 0) {
        // if price is the same, compare by id
        return orderA.id - orderB.id
    } else {
        return priceDiff
    }
}
