import React from 'react'
import {findMatches} from '../orders/OrderMatcher'

const DATE_TO_USE = new Date('2016')
global.Date = jest.fn(() => DATE_TO_USE)

it('does as the example instructs', () => {
    const buyOrders = [
        {id: 1002, type: "buy", quantity: 8, price: 470},
        {id: 1003, type: "buy", quantity: 5, price: 460},
    ]

    const sellOrders = [
        {id: 1001, type: "sell", quantity: 6, price: 480},
        {id: 1004, type: "sell", quantity: 10, price: 460},
    ]

    const matches = findMatches(sellOrders, buyOrders)

    expect(matches).toEqual([
        {
            created: DATE_TO_USE,
            price: 465, quantity: 8,
            buyOrder: {quantity: 8, price: 470, id: 1002, type: "buy"},
            sellOrder: {quantity: 10, price: 460, id: 1004, type: "sell"}
        },
        {
            created: DATE_TO_USE,
            price: 460, quantity: 2,
            buyOrder: {quantity: 5, price: 460, id: 1003, type: "buy"},
            sellOrder: {quantity: 2, price: 460, id: 1004, type: "sell"}
        }
    ])

    expect(buyOrders[0].quantity).toEqual(0)
    expect(buyOrders[1].quantity).toEqual(3)
    expect(sellOrders[1].quantity).toEqual(0)

})


