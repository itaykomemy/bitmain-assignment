import React from 'react'
import OrderSorter from '../orders/OrderSorter'

it('sorts by price then by id', () => {
    const orders = [
        {id: 1004, type: "buy", quantity: 5, price: 460},
        {id: 1003, type: "buy", quantity: 5, price: 460},
        {id: 1002, type: "buy", quantity: 8, price: 470},
        {id: 1005, type: "buy", quantity: 5, price: 460},
    ]

    const [first, second, third, fourth] = orders

    orders.sort(OrderSorter)

    expect(orders).toEqual(
        [third, second, first, fourth]
    )
})


