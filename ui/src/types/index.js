// @flow

export type OrderType = {
    id: number,
    type: string,
    quantity: number,
    price: number,
}

export type MatchType = {
    id: string,
    created: Date,
    quantity: number,
    price: number,
    buyOrder: OrderType,
    sellOrder: OrderType,
}