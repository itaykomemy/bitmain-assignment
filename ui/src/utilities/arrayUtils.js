const top = limit => (item, index) => index < limit

const top20 = top(20)
const top30 = top(30)

export {top20, top30}