import moment from 'moment/moment'

const formatDate = date => moment(date).format('LTS')

export {formatDate}